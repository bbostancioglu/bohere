package services

import models.DBM_PasswordInfo
import securesocial.core.PasswordInfo
import anorm.NotAssigned

object PasswordService {

    def save(passwordInfo: PasswordInfo): DBM_PasswordInfo = {
        DBM_PasswordInfo.save(new DBM_PasswordInfo(NotAssigned, passwordInfo.hasher, passwordInfo.password, passwordInfo.salt))
    }
}
