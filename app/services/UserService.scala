package services

import play.api.{Logger, Application}
import securesocial.core._
import securesocial.core.providers.Token
import securesocial.core.IdentityId
import models.{DBM_Token, DBM_PasswordInfo, DBM_User}
import anorm.{NotAssigned, Id}

class UserService(application: Application) extends UserServicePlugin(application) {

    def find(id: IdentityId): Option[Identity] = {
        if(id.userId forall Character.isDigit) {
            DBM_User.findById(Option(id.userId.toLong))
        } else {
            DBM_User.findByEmail(id.userId)
        }
    }

    def findByEmailAndProvider(email: String, providerId: String): Option[Identity] = {
        DBM_User.findByEmailAndProvider(email, providerId)
    }

    def save(user: Identity): Identity = {
        val passwordInfoId = user.passwordInfo match {
            case Some(passwordInfo) => PasswordService.save(passwordInfo).id.getOrElse(0.toLong)
            case None => 0
        }
        val oAuthInfoId = user.oAuth2Info match {
            case Some(oauthInfo) => OAuthService.save(oauthInfo).id.getOrElse(0.toLong)
            case None => 0
        }
        val dbmUser = DBM_User(NotAssigned, user.firstName, user.lastName,
            user.email, Option(passwordInfoId), "",
            user.authMethod.method, user.identityId.providerId, Option(oAuthInfoId), None
        )
        DBM_User.save(dbmUser)
    }

    def save(token: Token) = {
        DBM_Token.save(DBM_Token(NotAssigned, token.uuid, token.email, Option(token.creationTime.toDate),
            Option(token.expirationTime.toDate), token.isSignUp))
    }

    def findToken(uuid: String): Option[Token] = {
        Option(DBM_Token.token(DBM_Token.findByUuid(uuid)))
    }

    def deleteToken(uuid: String) {
        val token = DBM_Token.findByUuid(uuid)
        DBM_Token.delete(Option(token.get.id.get))
    }

    def deleteExpiredTokens() {
        val tokens = DBM_Token.all()
        tokens.foreach(token => if(DBM_Token.token(Option(token)).isExpired) DBM_Token.delete(Option(token.id.get)))
    }
}
