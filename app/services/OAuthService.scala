package services

import securesocial.core.{OAuth1Info, OAuth2Info}
import models.DBM_OAuthInfo
import anorm.NotAssigned

object OAuthService {

    def save(oauth2Info: OAuth2Info): DBM_OAuthInfo = {
        DBM_OAuthInfo.save(DBM_OAuthInfo(NotAssigned, oauth2Info.accessToken,
            oauth2Info.tokenType.get, oauth2Info.expiresIn, oauth2Info.refreshToken.get))
    }
}
