package models

import anorm._
import play.api.db._
import play.api.Play.current
import play.api.libs.json._

abstract class DBM_Base[T] {

    val tableName: String = null

    def parser: RowParser[T] = null

    def all(): List[T] = {
        DB.withConnection { implicit connection =>
            SQL("SELECT * FROM " + tableName).as(parser *)
        }
    }

    def findById(id: Option[Long]): Option[T] = {
        DB.withConnection { implicit connection =>
            SQL("SELECT * FROM " + tableName + " WHERE id = {id}").
                    on(
                'id -> id.get
            ).as(parser.singleOpt)
        }
    }

    def delete(id: Option[Long]) = {
        DB.withConnection { implicit connection =>
            SQL("DELETE FROM " + tableName + " WHERE id = {id}").on('id -> id.get).executeUpdate()
        }
    }

    implicit object PkFormat extends Format[Pk[Long]] {
        def reads(json: JsValue):JsResult[Pk[Long]] = JsSuccess(Id(json.as[Long]))
        def writes(id: Pk[Long]):JsNumber = JsNumber(id.get)
    }
}
