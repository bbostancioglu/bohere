package models

import java.util.Date
import anorm._
import anorm.SqlParser._
import securesocial.core._
import play.api.db._
import play.api.Play.current
import play.api.Logger
import play.api.libs.json._

case class DBM_User(id: Pk[Long] = NotAssigned,
                    firstName: String,
                    lastName: String,
                    email: Option[String],
                    passwordInfoId: Option[Long],
                    description: String,
                    authenticationMethod: String,
                    providerId: String,
                    oauthInfoId: Option[Long],
                    created: Option[Date]
                   ) extends Identity  {

    def fullName = s"$firstName $lastName"
    def oAuth1Info: Option[OAuth1Info] = None
    def oAuth2Info: Option[OAuth2Info] = if(oauthInfoId.get > 0) DBM_OAuthInfo.getOAuth2Info(DBM_OAuthInfo.findById(oauthInfoId).get) else None
    def avatarUrl: Option[String] = None
    def identityId: IdentityId = IdentityId(id.toString, providerId)
    def passwordInfo: Option[PasswordInfo] = if(passwordInfoId.get > 0) Option(DBM_PasswordInfo.passwordInfo(DBM_PasswordInfo.findById(passwordInfoId))) else None
    def authMethod: AuthenticationMethod = AuthenticationMethod(authenticationMethod)
}

object DBM_User extends DBM_Base[DBM_User] {

    override val tableName = "user_profile"

    override def parser = {
        get[Pk[Long]]("id") ~
        get[String]("firstName") ~
        get[String]("lastName") ~
        get[String]("email") ~
        get[Option[Long]]("passwordInfoId") ~
        get[String]("description") ~
        get[String]("authenticationMethod") ~
        get[String]("providerId") ~
        get[Option[Long]]("oauthInfoId") ~
        get[Option[Date]]("created") map {
            case id~firstName~lastName~email~passwordInfoId~description~authenticationMethod~providerId~oauthInfoId~created =>
                DBM_User(id, firstName, lastName, Option(email), passwordInfoId,
                    description, authenticationMethod, providerId, oauthInfoId, created)
        }
    }

    def findByEmailAndProvider(email: String, providerId: String): Option[DBM_User] = {
        DB.withConnection { implicit connection =>
            SQL("SELECT * FROM user_profile WHERE email = {email} AND providerId = {providerId}").
                    on(
                'email -> email,
                'providerId -> providerId
            ).as(parser.singleOpt)
        }
    }

    def findByEmail(email: String): Option[DBM_User] = {
        Logger.debug(email)
        DB.withConnection { implicit connection =>
            SQL("SELECT * FROM user_profile WHERE email = {email}").
                    on(
                'email -> email
            ).as(parser.singleOpt)
        }
    }

    def insert(user: DBM_User) = {
        DB.withConnection { implicit connection =>
            SQL(
                """
            INSERT INTO user_profile (firstName, lastName, email, passwordInfoId, description, authenticationMethod,
                                        providerId, oauthInfoId, created)
            VALUES ({firstName}, {lastName}, {email}, {passwordInfoId}, {description}, {authenticationMethod},
                                        {providerId}, {oauthInfoId}, {created})
                """
            ).on(
                'firstName -> user.firstName,
                'lastName -> user.lastName,
                'email -> user.email,
                'passwordInfoId -> user.passwordInfoId,
                'description -> user.description,
                'authenticationMethod -> user.authenticationMethod,
                'providerId -> user.providerId,
                'oauthInfoId -> user.oauthInfoId,
                'created -> user.created
            ).executeInsert(scalar[Long].single)
        }
    }

    def update(user: DBM_User) = {
        DB.withConnection { implicit connection =>
            SQL(
                """
                UPDATE user_profile set firstName = {firstName}, lastName = {lastName}, email = {email}, passwordInfoId = {passwordInfoId},
                    description = {description}, authenticationMethod = {authenticationMethod}, providerId = {providerId},
                    oauthInfoId = {oauthInfoId}, created = {created}
                WHERE id = {id}
                """
            ).on(
                'firstName -> user.firstName,
                'lastName -> user.lastName,
                'email -> user.email,
                'passwordInfoId -> user.passwordInfoId,
                'description -> user.description,
                'authenticationMethod -> user.authenticationMethod,
                'providerId -> user.providerId,
                'oauthInfoId -> user.oauthInfoId,
                'created -> user.created,
                'id -> user.id
            ).executeUpdate()
        }
    }

    def save(user: DBM_User): DBM_User = {
        val emailUser = findByEmail(user.email.get)
        if(user.id == NotAssigned && emailUser == None) {
            DBM_User(Id(insert(user)), user.firstName, user.lastName, user.email, user.passwordInfoId, user.description,
                    user.authenticationMethod, user.providerId, user.oauthInfoId, user.created)
        }
        else {
            update(emailUser.getOrElse(user))
            emailUser.getOrElse(user)
        }
    }
}