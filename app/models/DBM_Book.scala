package models

import java.util.Date
import anorm._
import anorm.SqlParser._
import play.api.libs.json._



case class DBM_Book(id: Pk[Long] = NotAssigned,
                    projectId: Option[Long],
                    title: String,
                    pageCount: Option[Long],
                    exportTime: Option[Date],
                    created: Option[Date]
                   )

object DBM_Book extends DBM_Base[DBM_Book] {

    override val tableName = "book"

    override def parser = {
        get[Pk[Long]]("id") ~
                get[Option[Long]]("projectId") ~
                get[String]("title") ~
                get[Option[Long]]("pageCount") ~
                get[Option[Date]]("exportTime") ~
                get[Option[Date]]("created") map {
            case id~projectId~title~pageCount~exportTime~created => DBM_Book(id, projectId, title, pageCount, exportTime, created)
        }
    }

    implicit val bookReads = Json.reads[DBM_Book]
    implicit val bookWrites = Json.writes[DBM_Book]
}
