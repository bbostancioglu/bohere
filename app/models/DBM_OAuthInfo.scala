package models

import anorm._
import anorm.SqlParser._
import securesocial.core.{OAuth2Info, OAuth1Info}
import play.api.db.DB
import anorm.~
import securesocial.core.OAuth2Info
import play.api.db._
import play.api.Play.current

case class DBM_OAuthInfo(   id: Pk[Long] = NotAssigned,
                            accessToken: String,
                            tokenType: String,
                            expiresIn: Option[Int],
                            refreshToken: String
                            )

object DBM_OAuthInfo extends DBM_Base[DBM_OAuthInfo] {

    override val tableName = "oauth_info"

    override def parser = {
        get[Pk[Long]]("id") ~
        get[String]("accessToken") ~
        get[String]("tokenType") ~
        get[Option[Int]]("expiresIn") ~
        get[String]("refreshToken") map {
            case id~accessToken~tokenType~expiresIn~refreshToken =>
                DBM_OAuthInfo(id, accessToken, tokenType, expiresIn, refreshToken)
        }
    }

    def getOAuth2Info(info: DBM_OAuthInfo): Option[OAuth2Info] =
        Option(OAuth2Info(info.accessToken, Option(info.tokenType), info.expiresIn, Option(info.refreshToken)))

    def insert(info: DBM_OAuthInfo) = {
        DB.withConnection { implicit connection =>
            SQL(
                """
            INSERT INTO oauth_info (accessToken, tokenType, expiresIn, refreshToken)
            VALUES ({accessToken}, {tokenType}, {expiresIn}, {refreshToken})
                """
            ).on(
                'accessToken -> info.accessToken,
                'tokenType -> info.tokenType,
                'expiresIn -> info.expiresIn,
                'refreshToken -> info.refreshToken
            ).executeInsert(scalar[Long].single)
        }
    }

    def update(info: DBM_OAuthInfo) = {
        DB.withConnection { implicit connection =>
            SQL(
                """
            UPDATE oauth_info accessToken = {accessToken}, tokenType = {tokenType},
                expiresIn = {expiresIn}, refreshToken = {refreshToken}
            WHERE id = {id}
                """
            ).on(
                'accessToken -> info.accessToken,
                'tokenType -> info.tokenType,
                'expiresIn -> info.expiresIn,
                'refreshToken -> info.refreshToken,
                'id -> info.id
            ).executeUpdate()
        }
    }

    def save(info: DBM_OAuthInfo): DBM_OAuthInfo = {
        if(info.id == NotAssigned) {
            DBM_OAuthInfo(Id(insert(info)), info.accessToken, info.tokenType, info.expiresIn, info.refreshToken)
        }
        else {
            update(info)
            info
        }
    }
}