package models

import anorm._
import java.util.Date
import org.joda.time.DateTime
import anorm.SqlParser._
import anorm.~
import securesocial.core.providers.Token
import play.api.db._
import play.api.Play.current
import java.text.DateFormat

case class DBM_Token(   id: Pk[Long] = NotAssigned,
                        uuid: String,
                        email: String,
                        creationTime: Option[Date],
                        expirationTime: Option[Date],
                        isSignUp: Boolean
                    )


object DBM_Token extends DBM_Base[DBM_Token] {

    override val tableName = "token"

    override def parser = {
        get[Pk[Long]]("id") ~
        get[String]("uuid") ~
        get[String]("email") ~
        get[Option[Date]]("creationTime") ~
        get[Option[Date]]("expirationTime") ~
        get[Boolean]("isSignUp") map {
            case id~uuid~email~creationTime~expirationTime~isSignUp =>
                DBM_Token(id, uuid, email, creationTime, expirationTime, isSignUp)
        }
    }

    def token(dbmToken: Option[DBM_Token]) = {
        Token(dbmToken.get.uuid, dbmToken.get.email, new DateTime(dbmToken.get.creationTime.get),
            new DateTime(dbmToken.get.expirationTime.get), dbmToken.get.isSignUp)
    }

    def findByUuid(uuid: String): Option[DBM_Token] = {
        DB.withConnection { implicit connection =>
            SQL("SELECT * FROM token WHERE uuid = {uuid}").
                    on(
                'uuid -> uuid
            ).as(parser.singleOpt)
        }
    }

    def insert(token: DBM_Token) = {
        DB.withConnection { implicit connection =>
            SQL(
                """
            INSERT INTO token (uuid, email, creationTime, expirationTime, isSignUp)
            VALUES ({uuid}, {email}, {creationTime}, {expirationTime}, {isSignUp})
                """
            ).on(
                'uuid -> token.uuid,
                'email -> token.email,
                'creationTime -> token.creationTime,
                'expirationTime -> token.expirationTime,
                'isSignUp -> token.isSignUp
            ).executeInsert(scalar[Long].single)
        }
    }

    def update(token: DBM_Token) = {
        DB.withConnection { implicit connection =>
            SQL(
                """
            UPDATE token uuid = {uuid}, email = {email}, creationTime = {creationTime},
            expirationTime = {expirationTime}, isSignUp = {isSignUp}
            WHERE id = {id}
                """
            ).on(
                'uuid -> token.uuid,
                'email -> token.email,
                'creationTime -> token.creationTime,
                'expirationTime -> token.expirationTime,
                'isSignUp -> token.isSignUp,
                'id -> token.id
            ).executeUpdate()
        }
    }

    def save(token: DBM_Token): DBM_Token = {
        if(token.id == NotAssigned) {
            DBM_Token(Id(insert(token)), token.uuid, token.email, token.creationTime, token.expirationTime, token.isSignUp)
        }
        else {
            update(token)
            token
        }
    }
}