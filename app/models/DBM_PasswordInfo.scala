package models

import anorm._
import anorm.SqlParser._
import securesocial.core.PasswordInfo
import anorm.~
import play.api.db._
import play.api.Play.current

case class DBM_PasswordInfo(
                           id: Pk[Long] = NotAssigned,
                           hasher: String,
                           password: String,
                           salt: Option[String])


object DBM_PasswordInfo extends DBM_Base[DBM_PasswordInfo] {

    override val tableName = "password_info"

    override def parser = {
        get[Pk[Long]]("id") ~
        get[String]("hasher") ~
        get[String]("password") ~
        get[Option[String]]("salt") map {
            case id~hasher~password~salt =>
                DBM_PasswordInfo(id, hasher, password, salt)
        }
    }

    def passwordInfo(dbmPasswordInfo: Option[DBM_PasswordInfo]) = {
        PasswordInfo(dbmPasswordInfo.get.hasher, dbmPasswordInfo.get.password, dbmPasswordInfo.get.salt)
    }

    def insert(passwordInfo: DBM_PasswordInfo) = {
        DB.withConnection { implicit connection =>
            SQL(
                """
            INSERT INTO password_info (hasher, password, salt)
            VALUES ({hasher}, {password}, {salt})
                """
            ).on(
                'hasher -> passwordInfo.hasher,
                'password -> passwordInfo.password,
                'salt -> passwordInfo.salt
            ).executeInsert(scalar[Long].single)
        }
    }

    def update(passwordInfo: DBM_PasswordInfo) = {
        DB.withConnection { implicit connection =>
            SQL(
                """
            UPDATE password_info hasher = {hasher}, password = {password}, salt = {salt}
            WHERE id = {id}
                """
            ).on(
                'hasher -> passwordInfo.hasher,
                'password -> passwordInfo.password,
                'salt -> passwordInfo.salt,
                'id -> passwordInfo.id
            ).executeUpdate()
        }
    }

    def save(info: DBM_PasswordInfo): DBM_PasswordInfo = {
        if(info.id == NotAssigned) {
            DBM_PasswordInfo(Id(insert(info)), info.hasher, info.password, info.salt)
        }
        else {
            update(info)
            info
        }
    }
}
