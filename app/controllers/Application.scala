package controllers

import play.api.mvc._
import securesocial.core.SecureSocial
import securesocial.core.{Identity, Authorization}
import models.DBM_User

object Application extends Controller with SecureSocial {

    def index = SecuredAction { implicit request =>
        Ok(views.html.index(request.user.toString))
    }

//    def index = UserAwareAction { implicit request =>
//        val userName = request.user match {
//            case Some(user) => user.fullName
//            case _ => "guest"
//        }
//        Ok(views.html.index("Hello %s".format(userName)))
//    }
}