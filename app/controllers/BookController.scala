package controllers

import play.api._
import play.api.mvc._
import models.DBM_Book
import play.libs.Json

object BookController extends Controller {

    def getById(id: String) = Action {
//        Ok(Json.toJson(DBM_Book.findById(Option(id.toLong))).toString)
        Ok(Json.toJson(DBM_Book.findById(Option(id.toLong)).get).toString)
        //http://stackoverflow.com/questions/9610103/scala-2-10-its-impact-on-json-libraries-and-case-class-validation-creation
    }
}