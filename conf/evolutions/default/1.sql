# DBM_Book schema
# --- !Ups
CREATE TABLE book (
    id bigserial primary key,
    projectId integer NOT NULL,
    title varchar(256) NOT NULL,
    pageCount integer DEFAULT 0,
    exportTime timestamp DEFAULT NULL,
    created timestamp DEFAULT CURRENT_TIMESTAMP
);
# --- !Downs
DROP TABLE book;


# DBM_User schema
# --- !Ups
CREATE TABLE user_profile (
    id bigserial primary key,
    firstName character varying(255),
    lastName character varying(255),
    email character varying(255),
    passwordInfoId integer,
    description text,
    authenticationMethod character varying(255),
    providerId character varying(255),
    oauthInfoId integer,
    created timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);
# --- !Downs
DROP TABLE user_profile;


# DBM_OAuthInfo schema
# --- !Ups
CREATE TABLE oauth_info (
    id bigserial primary key,
    accessToken character varying(512),
    tokenType character varying(255),
    expiresIn integer,
    refreshToken character varying(512)
);
# --- !Downs
DROP TABLE oauth_info;


# DBM_PasswordInfo schema
# --- !Ups
CREATE TABLE password_info (
    id bigserial primary key,
    hasher character varying(255),
    password character varying(512),
    salt character varying(512)
);
# --- !Downs
DROP TABLE password_info;


# DBM_Token schema
# --- !Ups
CREATE TABLE token (
    id bigserial primary key,
    uuid character varying(512),
    email character varying(255),
    creationTime timestamp without time zone NOT NULL,
    expirationTime timestamp without time zone NOT NULL,
    isSignUp boolean
);
# --- !Downs
DROP TABLE token;





